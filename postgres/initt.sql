CREATE USER deeppointuser with password 'deeppointpass';
CREATE DATABASE deeppointdb;
GRANT ALL PRIVILEGES ON DATABASE deeppointdb TO deeppointuser;
