from django.db import models


# Create your models here.


class Subscriber(models.Model):
    email = models.EmailField(null=True, blank=True)
    created = models.DateTimeField(auto_created=True)

    def __str__(self):
        return self.email


class Contacter(models.Model):
    name = models.TextField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    subject = models.TextField(blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_created=True)

    def __str__(self):
        return '{} {}'.format(self.email, self.name)
