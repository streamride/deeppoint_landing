import datetime

from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view

from .models import Subscriber, Contacter


@api_view(['POST'])
@csrf_exempt
def contacts(request):
    if request.method == 'POST':
        data = request.POST
        email = data.get('email', 'none@email.com')
        name = data.get('name', '')
        message = data.get('message', '')
        subject = data.get('subject', '')
        contacter = Contacter()
        contacter.email = email
        contacter.name = name
        contacter.subject = subject
        contacter.message = message
        contacter.created = datetime.datetime.now()
        contacter.save()
        return HttpResponse(status=status.HTTP_201_CREATED)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST'])
@csrf_exempt
def subscribe(request):
    if request.method == 'POST':
        data = request.POST
        email = data.get('email', 'none@email.com')
        subscriber = Subscriber()
        subscriber.email = email
        subscriber.created = datetime.datetime.now()
        subscriber.save()
        return HttpResponse(status=status.HTTP_201_CREATED)
    else:
        return HttpResponse(status=status.HTTP_405_METHOD_NOT_ALLOWED)
