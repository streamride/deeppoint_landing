from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'contact', views.contacts, name='contacts'),
    url(r'subscribe', views.subscribe, name='subscribe'),
]
